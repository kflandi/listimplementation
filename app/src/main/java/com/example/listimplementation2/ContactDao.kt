package com.example.listimplementation2

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDao {

    @Upsert
    suspend fun upsertContact(name: Name)

    @Delete
    suspend fun deleteContact(name: Name)

    @Query("SELECT * FROM name ORDER BY title ASC")
    fun getContactsOrderedByFirstName(): Flow<List<Name>>



   // @Query("SELECT * FROM contact ORDER BY phoneNumber ASC")
    //fun getContactsOrderedByPhoneNumber(): Flow<List<Contact>>
}