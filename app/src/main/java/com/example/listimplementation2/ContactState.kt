package com.example.listimplementation2

data class ContactState(
    val names: List<Name> = emptyList(),
    val title: String="" ,
   // val phoneNumber: String ="" ,
    val isAddingContact: Boolean = false,
    val sortType: SortType = SortType.TITLE
)
