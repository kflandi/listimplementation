package com.example.listimplementation2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@OptIn(ExperimentalCoroutinesApi::class)
class ContactViewModel(
    private val dao: ContactDao
): ViewModel() {

    private val _sortType = MutableStateFlow(SortType.TITLE)
    private val _names = _sortType
        .flatMapLatest { sortType ->
            when(sortType) {
                SortType.TITLE -> dao.getContactsOrderedByFirstName()

               // SortType.PHONE_NUMBER -> dao.getContactsOrderedByPhoneNumber()
            }
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

    private val _state = MutableStateFlow(ContactState())
    val state = combine(_state, _sortType, _names) { state, sortType, names ->
        state.copy(
            names = names,
            sortType = sortType
        )
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), ContactState())

    fun onEvent(event: ContactEvent) {
        when(event) {
            is ContactEvent.DeleteContact -> {
                viewModelScope.launch {
                    dao.deleteContact(event.name)
                }
            }
            ContactEvent.HideDialog -> {
                _state.update { it.copy(
                    isAddingContact = false
                ) }
            }
            ContactEvent.SaveContact -> {
                val title = state.value.title

                //val phoneNumber = state.value.phoneNumber
// || phoneNumber.isBlank()  (<- add below next to firstName if date added is a sort method
                if(title.isBlank()  ) {
                    return
                }

                val name = Name(
                    title = title,

                   // phoneNumber = phoneNumber
                )
                viewModelScope.launch {
                    dao.upsertContact(name)
                }
                _state.update { it.copy(
                    isAddingContact = false,
                    title = "",

                  //  phoneNumber = ""
                ) }
            }
            is ContactEvent.SetTitle -> {
                _state.update { it.copy(
                    title = event.title
                ) }
            }
/*               add if date added is accepted
            is ContactEvent.SetPhoneNumber -> {
                _state.update { it.copy(
                    phoneNumber = event.phoneNumber
                ) }
            }
*/            ContactEvent.ShowDialog -> {
                _state.update { it.copy(
                    isAddingContact = true
                ) }
            }
            is ContactEvent.SortContacts -> {
                _sortType.value = event.sortType
            }
        }
    }
}