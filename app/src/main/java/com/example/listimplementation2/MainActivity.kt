package com.example.listimplementation2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.listimplementation2.ui.theme.ListImplementation2Theme

class MainActivity : ComponentActivity() {


        private val db by lazy {
            Room.databaseBuilder(
                applicationContext,
                ContactDatabase::class.java,
                "titles.db"
            ).build()
        }
        private val viewModel by viewModels<ContactViewModel>(
            factoryProducer = {
                object: ViewModelProvider.Factory{
                    override fun <T : ViewModel> create(modelClass: Class<T>): T {
                        return ContactViewModel(db.dao) as T
                    }
                }
            }
        )



        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ListImplementation2Theme {
                // A surface container using the 'background' color from the theme
                val state by viewModel.state.collectAsState()
                ContactScreen(state = state, onEvent = viewModel::onEvent)

            }
        }
    }
}

