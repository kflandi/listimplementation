package com.example.listimplementation2

sealed interface ContactEvent {
    object SaveContact: ContactEvent
    data class SetTitle(val title: String): ContactEvent
    //data class SetPhoneNumber(val phoneNumber: String): ContactEvent
    object ShowDialog: ContactEvent
    object HideDialog: ContactEvent
    data class SortContacts(val sortType: SortType): ContactEvent
    data class DeleteContact(val name: Name): ContactEvent
}